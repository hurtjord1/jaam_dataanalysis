# JAAM_DataAnalysis

The JAAM solution is a platform that identifies individuals' music tastes, compares it to other people, and then recommends a shared playlist that caters to the tastes of everyone in a group. Unlike current streaming services and their generic recommended playlists.

This project contains the data science part. It is split into a several Python notebooks covering all stages of the process.
This is the test use case from the co-founders of this undertaking: Jordi, Allison, Annalaura, Mahmoud.
